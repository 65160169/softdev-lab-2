/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.softdevlab2;
/*Thanaphat Ketsani 65160169*/
import java.util.Scanner;
public class Softdevlab2 {
    static char[][] table = {{'_','_','_'},{'_','_','_'},{'_','_','_'}};
    static char ox = 'X';
    static String status = "playing";
    static char winner = '0';
    static int row,col;
    static int count = 0;
    
    public static void showtable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    
    public static void input(){
        Scanner kb = new Scanner(System.in);
        System.out.print("turn "+ox+" input row,col: ");
        row = kb.nextInt(); col = kb.nextInt();
    }
    
    public static void check_(){
        if(table[row-1][col-1] == '_'){
            table[row-1][col-1] = ox;
            count += 1;
            System.out.println();
        }else if(table[row-1][col-1] != '_'){
            input();
        }
    }
    
    public static void checkrow(){
        for(int i = 0;i<3;i++){
            if(table[i][0] == 'X' && table[i][1] == 'X' && table[i][2] == 'X'){
                winner = 'X';
            }else if(table[i][0] == 'O' && table[i][1] == 'O' && table[i][2] == 'O'){
                winner = 'O';
            }else if(count==9){
                winner = 'd';
            }
        }
    }
    
    public static void checkcol(){
        for(int i = 0;i<3;i++){
            if(table[0][i] == 'X' && table[1][i] == 'X' && table[2][i] == 'X'){
                winner = 'X';
            }else if(table[0][i] == 'O' && table[1][i] == 'O' && table[2][i] == 'O'){
                winner = 'O';
            }
        }
    }
    
    public static void checkspalnt(){
            if(table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'){
                winner='O';
            }else if(table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O'){
                winner='O';
            }else if(table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'){
                winner='X';
            }else if(table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X'){
                winner='X';
            }
    }
    
    public static void checkdraw(){
        if(count==9){
                winner = 'd';
            }
        if(winner == 'd'){
                System.out.println("Draw!!");
            }
    }
    
    public static void switchox(){
        if(ox == 'X'){ox = 'O';}
        else if(ox == 'O'){ox = 'X';}
    }
    
    public static void showwinner(){
        if(winner!='0'){
            showtable();
            System.out.println("The winner is "+winner);
        }
    }
    
    public static void restartcheck(){
        Scanner kb = new Scanner(System.in);
        if(winner == 'X' || winner == 'O'){
            System.out.print("Want to Continue?(y/n): ");
            char yn = kb.next().charAt(0);
            if(yn == 'y'){
                System.out.print("New Game Start");
            for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                    table[i][j]='_';
                }
                System.out.println();
            }
            count=0;
            ox = 'X';
            status = "playing";
            winner = '0';
            }else if(yn == 'n'){
                status = "over";
            }
        }
    }
    public static void main(String[] args) {
        System.out.println("Welcome To My Game OX");
        while(status.equals("playing")){
           
            showtable();
            input();
            check_();
            checkrow();
            checkcol();
            checkspalnt();
            checkdraw();
            switchox();
            showwinner();
            restartcheck();
        }
        
    }
}
